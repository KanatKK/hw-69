import React, {useEffect, useReducer} from 'react';
import './searchShows.css'
import axios from 'axios';
import {NavLink} from "react-router-dom";

const initialState = {
    currentShow: '',
    shows: null,
    display: 'none',
};

const GET_SHOWS = 'GET_SHOWS';
const CURRENT_SHOW = 'CURRENT_SHOW';
const OPEN_AUTO_COMPLETE = 'OPEN_AUTO_COMPLETE';
const CLOSE_AUTO_COMPLETE = 'CLOSE_AUTO_COMPLETE'

const addShows = (value) => {
    return {type: GET_SHOWS, value};
};

const currentShow = (value) => {
    return {type: CURRENT_SHOW, value};
};

const openAutoComplete = () => {
    return {type: OPEN_AUTO_COMPLETE};
};

const closeAutoComplete = () => {
    return {type: CLOSE_AUTO_COMPLETE};
};

const reducer = (state, action) => {
    switch (action.type) {
        case GET_SHOWS:
            return {
                ...state,
                shows: action.value
            };
        case CURRENT_SHOW:
            return {
                ...state,
                currentShow: action.value
            };
        case OPEN_AUTO_COMPLETE:
            return {
                ...state,
                display: 'block'
            };
        case CLOSE_AUTO_COMPLETE:
            return {
                ...state,
                display: 'none'
            }
        default:
            return state;
    }
};

const SearchShows = () => {
    const [state, dispatch] = useReducer(reducer, initialState);

    const currentShowHandler = event => {
        dispatch(currentShow(event.target.value));
        dispatch(openAutoComplete());
    };

    useEffect(() => {
        const getShows = async () => {
            const response = await axios.get('http://api.tvmaze.com/search/shows?q=' + state.currentShow);
            dispatch(addShows(response.data));
        };
        getShows().catch();
    }, [dispatch, state.currentShow]);

    const hideAutoComplete = () => {
        dispatch(closeAutoComplete());
    };

    return (
        <div className="container">
            <header>
                <p className="headerTxt">TV Shows</p>
            </header>
            <label className="searchLine">
                Search for TV Shows:
                <input type="text" onChange={currentShowHandler}/>
                <div className="responses" style={{display: state.display}}>
                    <div className="autoComplete">{state.shows && state.shows.map(item => {
                        return <NavLink key={item.show.id} to={`/show/${item.show.id}`}>
                            <div className="shows" onClick={hideAutoComplete}>{item.show.name}</div>
                        </NavLink>})}
                    </div>
                </div>
            </label>
        </div>
    );
};

export default SearchShows;