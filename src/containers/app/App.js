import React from 'react';
import './App.css';
import {Route, Switch, BrowserRouter} from 'react-router-dom';
import SearchShows from "../searchShows/searchShows";
import Show from "../../components/show/Show";

const App = () => {
  return (
      <BrowserRouter>
          <Route component={SearchShows}/>
          <Switch>
              <Route path="/show/:id" component={Show}/>
          </Switch>
      </BrowserRouter>
  );
};

export default App;
