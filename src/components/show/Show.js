import React, {useReducer, useEffect} from 'react';
import './Show.css';
import axios from 'axios';
import ReactHtmlParser from 'react-html-parser';

const initialState = {
    show: null
};

const GET_SHOW = 'GET_SHOW';

const addShow = (value) => {
    return {type: GET_SHOW, value};
};

const reducer = (state, action) => {
    switch (action.type) {
        case GET_SHOW:
            return {
                ...state,
                show: action.value
            }
        default:
            return state;
    }
};

const Show = props => {
    const [state, dispatch] = useReducer(reducer, initialState);

    useEffect(() => {
        const getShow = async () => {
            const response = await axios.get('http://api.tvmaze.com/shows/' + props.match.params.id);
            dispatch(addShow(response.data));
        };
        getShow().catch();
    }, [props.match.params.id]);

    if (state.show !== null) {
        return (
            <div className="container">
                <div className="show">
                    <h1 className="showName">{state.show.name}</h1>
                    <div className="content">
                        <div className="img">
                            {state.show.image
                            &&
                            <img src={state.show.image.original} className="poster" alt="Poster"/>}
                        </div>
                        <div className="info">
                            <p className="type">{'Type: ' + state.show.type}</p>
                            <p>{'Language: ' + state.show.language}</p>
                            <p>{'Genres: ' + state.show.genres + ', '}</p>
                            <p>{ReactHtmlParser(state.show.summary)}</p>
                        </div>
                    </div>
                </div>
            </div>
        );
    } else {
        return null;
    }
};

export default Show;